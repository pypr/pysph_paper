if __name__ == "__main__":
    import sys
    omp = sys.argv[1]
    import matplotlib
    import numpy as np
    matplotlib.use('Tkagg')
    from matplotlib import pyplot as plt
    plt.figure(figsize=(5, 4))
    if omp == 'omp':
        threads, time1, time2 = np.loadtxt('omp.txt', unpack=True)
        plt.plot(threads, time1[0]/time1, '-o', label='N=1.4M')
        plt.plot(threads, time2[0]/time2, '-o', label='N=0.17M')
        plt.xlabel('threads')
    else:
        core, time = np.loadtxt('mpi.txt', unpack=True)
        plt.plot(core, time[0]/time, '-o', label='N=5.5M')
        plt.plot(core, core, '-o', label='ideal')
        plt.xlabel('cores')
    plt.ylabel('speed-up')
    plt.legend()
    plt.grid()
    plt.tight_layout()
    if omp == 'omp':
        plt.savefig('omp_speedup.pdf')
    else:
        plt.savefig('mpi_speedup.pdf')
    plt.close()

