\section{Introduction}
\label{sec:intro}

Particle methods are a class of numerical methods where particles are used to
carry physical properties. These properties evolve as per the governing
differential equations of the problem. They may be used to simulate continuum
mechanics problems which is our present interest. Particle methods are
typically Lagrangian and meshless and this allows the method to handle
problems with free-surfaces and large deformations. The Smoothed Particle
Hydrodynamics method (SPH) was independently developed by \citet{lucy77} and
\citet{monaghan-gingold-stars-mnras-77}. The method has since developed to
simulate a wide variety of problems in continuum mechanics.

A typical meshless particle method implementation requires the following,
\begin{itemize}
\item representation of the field properties in terms of particles.
\item reconstruction of the field properties and their derivatives using the
  particle positions and properties alone.
\item identification of neighboring particles.
\item evolution of the particle properties over time.
\end{itemize}

There are various open-source software packages like
DualSphysics~\cite{crespo2015dualsphysics},
GADGET-4~\cite{springel2020simulating},
AquaGPUSph~\cite{cercos2015aquagpusph}, PHANTOM~\cite{phantom:2018},
SPlisHSPlasH~\cite{koschier_2019} among others which solve SPH problems
efficiently. A fairly exhaustive listing of open source and commercial
packages is provided in the review by \citet{Shadloo16}. In
Table~\ref{table:compare} many open source SPH packages and their features are
listed. The footnotes explain the respective columns.

\input{software_comparison}

From the Table~\ref{table:compare} it can be seen that all of these packages
(except PySPH) are implemented in a lower-level programming language like C,
C++, or FORTRAN. While some of these feature a Python interface, the primary
programming language used to implement the schemes is still in a lower-level
language. One can also see that apart from the PHANTOM package, the other
packages do not employ any form of continuous integration testing that is
public. Studies~\cite{prechelt_empirical_2000,gmys_comparative_2020} have
indicated that higher-level programming languages like Python~\cite{py:python}
lead to more programmer productivity. Python is also acknowledged by these
studies and other articles~\cite{py:nature:2015} as a popular language and as
a good introductory programming language~\cite{py:teaching-us}. Our own
experience has indicated that the use of a high-level language makes the code
a lot more accessible to students and researchers. Python features extensive
libraries for scientific computation, general purpose tasks, and easily
interfaces with lower-level languages like C/C++, FORTRAN as well as OpenCL and
CUDA. These reasons make it an ideal choice for use in PySPH.

Many of the existing SPH related packages are developed with a few particular
SPH schemes or application domains in mind. For example, many of the packages
mentioned above do not solve elastic dynamics and while some solve
incompressible fluid mechanics problems they do not support compressible fluid
flow and vice-versa. In addition, there are other meshless methods like the
Discrete Element Method (DEM)~\cite{cundall1979discrete}, Reproducing Kernel
Particle Method (RKPM)~\cite{liu:rkpm:95}, Moving Least-Squares Particle
Hydrodynamics (MLSPH)~\cite{dilts:mls:99}, and others
(see~\cite{nguyen:review:2008} for a review of meshless methods). Researchers
are continuing to explore new methods and techniques in order to improve their
efficiency and accuracy. It is difficult to perform research in a new area
without investing a significant amount of time in building the necessary
tools. Our goal in creating PySPH is to make it possible for researchers to
build upon the work of others with relative ease. To this end PySPH provides,
\begin{itemize}
\item data-structures to store particle properties irrespective of the
  meshless method being used.
\item algorithms to find nearest neighbors of particles for various uses.
\item the ability to write high-level Python code to express the
  inter-particle interactions and have them seamlessly execute on multi-core
  CPUs and GPUs.
\item implementation in the form of reusable libraries. This is in contrast
  with tools that only seek to solve specific problems.
\end{itemize}
Furthermore, to promote usability, extensibility, and quality, PySPH
\begin{itemize}
\item is distributed under a liberal, open-source, BSD (three-clause) license.
  This does not restrict commercial use of the software.
\item is a cross platform package that works on the major platforms today
  viz.\ Linux, Windows, and MacOS.
\item is written using modern software engineering practices including public
  hosting on \url{https://github.com/pypr/pysph}, easily accessible
  documentation, the use of unit tests, and continuous integration testing.
\item provides a large number of well established schemes.
\item provides an extensive collection of reusable benchmark and example
  problems.
\item uses a high-level and easy to read programming language,
  Python~\cite{py:python}.
\end{itemize}

In order to promote reproducible research, PySPH is relatively easy to install
using standard tools available in the Python ecosystem. This allows
researchers to share their relevant changes alone which in combination with
PySPH can be used to reproduce results. PySPH seeks to offer these features
without sacrificing computational performance. This is important since SPH
simulations can be computationally demanding. PySPH therefore runs on
multi-core CPUs, GPUs and also supports distributed execution on clusters.
PySPH supports these without requiring any major changes to the user code.
PySPH was originally designed to run in serial on CPUs and be distributed
using MPI. In 2013 it was redesigned and reimplemented to make use of
automatic code generation from high-level Python. This made PySPH much easier
to use and also demonstrated improved performance. Some historical details and
design decisions are mentioned in~\cite{PR:pysph:scipy16}.

PySPH uses object-orientation for maximum reuse and managing complexity.
Python is used to specify the inter-particle interactions and high-performance
code is generated from this high-level specification. Source templates are
used to abstract any common patterns and generate code. These features allow
for a great deal of flexibility without sacrificing much performance.

While PySPH has been used extensively by our group, it has been used in
several recent papers by other researchers in the
community~\cite{meyer_parameter_2020,arai_comparison_2020,li_smoothed_2018,bao_modified_2019}.

The next section outlines the basic equations employed in SPH in order to
provide a brief background. We then discuss the design of the PySPH framework
by showing how one may solve a simple problem with it. We use this example to
highlight the features and design of PySPH. We then show several sample
applications from PySPH to benchmark and demonstrate its wide applicability.
We also discuss the computational performance of PySPH.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% End:
