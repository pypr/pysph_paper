\documentclass[manuscript,screen]{acmart}

\AtBeginDocument{%
  \providecommand\BibTeX{{%
    \normalfont B\kern-0.5em{\scshape i\kern-0.25em b}\kern-0.8em\TeX}}}

\setcopyright{acmcopyright}
\copyrightyear{2020}
\acmYear{2020}
\acmDOI{XXX} %10.1145/1122445.1122456}

%\usepackage[hidelinks]{hyperref}
\usepackage{pgf}
\usepackage{subcaption}
\usepackage{amsmath}
\usepackage{multirow}
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{footnote}
\makesavenoteenv{tabular}
\makesavenoteenv{table}

% For the TODOs
\usepackage{xcolor}
\usepackage{xargs}
\usepackage[colorinlistoftodos,textsize=footnotesize]{todonotes}
\newcommand{\todoin}{\todo[inline]}
% from here: https://tex.stackexchange.com/questions/9796/how-to-add-todo-notes
\newcommandx{\unsure}[2][1=]{\todo[linecolor=red,backgroundcolor=red!25,bordercolor=red,#1]{#2}}
\newcommandx{\change}[2][1=]{\todo[linecolor=blue,backgroundcolor=blue!25,bordercolor=blue,#1]{#2}}
\newcommandx{\info}[2][1=]{\todo[linecolor=OliveGreen,backgroundcolor=OliveGreen!25,bordercolor=OliveGreen,#1]{#2}}

%Boldtype for greek symbols
\newcommand{\teng}[1]{\ensuremath{\boldsymbol{#1}}}
\newcommand{\ten}[1]{\ensuremath{\mathbf{#1}}}

\lstset{language=Python,
  basicstyle=\ttfamily\bfseries\small,
  commentstyle=\color{red}\itshape,
  stringstyle=\ttfamily\color{green!50!black},
  showstringspaces=false,
  keywordstyle=\color{blue}\bfseries}

\usepackage{tikz}
\usetikzlibrary{arrows.meta}
\usetikzlibrary{shapes.geometric}
\usetikzlibrary{fit}

\tikzset{%
  >={Latex[width=2mm,length=2mm]},
  % Specifications for style of nodes:
            base/.style = {rectangle, rounded corners, draw=black,
                           minimum width=4cm, minimum height=1cm,
                           text centered, font=\sffamily},
  activityStarts/.style = {base, fill=blue!30},
       startstop/.style = {base, fill=red!30},
       activityRuns/.style = {base, fill=green!30},
       optional/.style = {base, fill=lightgray},
         process/.style = {base, minimum width=2.5cm, fill=orange!15,
           font=\ttfamily},
         decision/.style = {diamond, minimum width=3cm, text centered, draw=black, fill=green!30}
}

%\newcommand{\typ}[1]{\textbf{\texttt{{#1}}}}
\newcommand{\code}[1]{\lstinline{#1}}
\newcommand{\codeblack}[1]{\lstinline[keywordstyle=\color{black}]{#1}}

\usepackage{lineno}

% for tables
\usepackage{booktabs}
\usepackage{adjustbox}
% https://www.sascha-frank.com/Faq/tables_six.html
\usepackage{array}
\newcolumntype{L}[1]{>{\raggedright\arraybackslash}p{#1}}


\begin{document}

\title{PySPH: a Python-based framework for smoothed particle hydrodynamics}
\author{Prabhu Ramachandran}
\authornote{Corresponding author}
\authornote{Authors in rough order of contributions to software and manuscript.}
\email{prabhu@aero.iitb.ac.in}
\orcid{0000-0001-6337-1720}
\author{Aditya Bhosale}
\email{adityapb1546@gmail.com}
\author{Kunal Puri}
\email{kunal.puri@numeca.be}
\author{Pawan Negi}
\email{pawan.n@aero.iitb.ac.in}
\author{Abhinav Muta}
\email{mutaabhinav@gmail.com}
\author{A Dinesh}
\email{adepu.dinesh.a@gmail.com}
\author{Dileep Menon}
\email{dileepsam92@gmail.com}
\author{Rahul Govind}
\email{rahulgovind517@gmail.com}
\author{Suraj Sanka}
\email{sankasuraj@gmail.com}
\author{Amal S Sebastian}
\email{amalssebastian@gmail.com}
\author{Ananyo Sen}
\email{ananyo.sen2@gmail.com}
\author{Rohan Kaushik}
\email{rohankaush@gmail.com}
\author{Anshuman Kumar}
\email{anshu266man@gmail.com}
\author{Vikas Kurapati}
\email{vikky.kurapati@gmail.com}
\author{Mrinalgouda Patil}
\email{mpcsdspa@gmail.com}
\author{Deep Tavker}
\email{tavkerdeep@gmail.com}
\author{Pankaj Pandey}
\email{pankaj86@gmail.com}
\author{Chandrashekhar Kaushik}
\email{shekhar.kaushik@gmail.com}
\author{Arkopal Dutt}
\email{arkopal.dutt@gmail.com}
\author{Arpit Agarwal}
\email{arpit.r.agarwal@gmail.com}
\affiliation{
  \institution{Indian Institute of
    Technology Bombay}
  \streetaddress{Department of Aerospace Engineering, IIT Bombay, Powai}
  \city{Mumbai}
  \postcode{400076}
}

\renewcommand{\shortauthors}{Ramachandran and Bhosale, et al.}

\begin{abstract}
  PySPH is an open-source, Python-based, framework for particle methods in
  general and Smoothed Particle Hydrodynamics (SPH) in particular. PySPH
  allows a user to define a complete SPH simulation using pure Python.
  High-performance code is generated from this high-level Python code and
  executed on either multiple cores, or on GPUs, seamlessly. It also supports
  distributed execution using MPI. PySPH supports a wide variety of SPH
  schemes and formulations. These include, incompressible and compressible
  fluid flow, elastic dynamics, rigid body dynamics, shallow water equations,
  and other problems. PySPH supports a variety of boundary conditions
  including mirror, periodic, solid wall, and inlet/outlet boundary
  conditions. The package is written to facilitate reuse and reproducibility.
  This paper discusses the overall design of PySPH and demonstrates many of
  its features. Several example results are shown to demonstrate the range of
  features that PySPH provides.
\end{abstract}

 \begin{CCSXML}
<ccs2012>
<concept>
<concept_id>10002950.10003705.10003707</concept_id>
<concept_desc>Mathematics of computing~Solvers</concept_desc>
<concept_significance>500</concept_significance>
</concept>
<concept>
<concept_id>10010405.10010432.10010441</concept_id>
<concept_desc>Applied computing~Physics</concept_desc>
<concept_significance>500</concept_significance>
</concept>
</ccs2012>
\end{CCSXML}

\ccsdesc[500]{Mathematics of computing~Solvers}
\ccsdesc[500]{Applied computing~Physics}

%% keywords here, in the form: keyword \sep keyword
\keywords{{PySPH}, {Smoothed particle hydrodynamics}, {open source}, {Python}, {GPU}, {CPU}}

\maketitle
% \linenumbers

\input{intro}

\input{sph}

\input{design}

\input{results}

\input{conclusions}

%\section*{References}
\bibliographystyle{model6-num-names}
\bibliography{references}


\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% fill-column: 78
%%% End:
