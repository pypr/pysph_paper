\section{The SPH method}\label{sec:sph}

In this section we briefly introduce the SPH method. We refer the reader
to~\cite{violeau_book_2012} for a detailed discussion. SPH is a Lagrangian,
meshless, particle method, where the particles represent the underlying
continuum. In SPH, a field variable $f(\ten{x})$ over a domain
$\Omega$ can be approximated by a convolution with the kernel function
$W(\ten{x}, h)$:
%
\begin{equation}
  \label{eq:sph:interpolation}
  f(\ten{x}) = \int_{\Omega} f(\ten{y}) W(\ten{x} - \ten{y}, h) \text{d}\ten{y},
\end{equation}
%
where $h$ is the smoothing length, and $\text{d}\ten{y}$ is the differential
volume element. The kernel function approximates the delta function in the
limit $h \rightarrow 0$ and is a compact function such that $W(\ten{r}) = 0$
when $|\ten{r}| > kh$ for some constant $k$. To obtain the discrete counterpart the
field is discretized into particles having mass $m_i$, density $\rho_i$, and
the differential volume is replaced by the volume $V_i = m_i/\rho_i$ where the
subscript denotes the index of the $i^{th}$ particle at position $\ten{x}_i$.
The integral is then approximated by a summation:
%
\begin{equation}
  \label{eq:sph:interpolation:sum}
  f(\ten{x}_i) = \sum_{j} V_j f(\ten{x}_j) W(\ten{x}_i - \ten{x}_j, h),
\end{equation}
%
where the summation is over all the particle in the support of the kernel
function.  If the function $f(\ten{x}_i)$ is replaced by density
$\rho(\ten{x}_i)$ we get the summation density formula in SPH,
%
\begin{equation}
  \label{eq:2}
  \rho(\ten{x}_i) = \sum_{j} m_j W(\ten{x}_i - \ten{x}_j, h).
\end{equation}
%
The SPH approximation can be used to discretize partial differential equations.
The governing equations of motion for a fluid flow, expressed in the Lagrangian
form, are,
\begin{equation}
  \label{eq:ns:mass}
   \frac{d\rho}{dt} = - \rho \nabla \cdot \ten{u},
\end{equation}
%
\begin{equation}
  \label{eq:ns:mom}
  \frac{d\ten{u}}{dt} = - \frac{1}{\rho}\nabla p +
  \nu \nabla^2 \ten{u} + \ten{g},
\end{equation}
%
where $\frac{d(.)}{dt}$ is the material derivative, $\rho$ is the density, $p$
is the pressure, $\nu$ is the kinematic viscosity, and $\ten{g}$ is the
external acceleration. In the case of a weakly-compressible fluid, the
equation of state which completes the above equations is,
%
\begin{equation}
  \label{eq:eos}
  p = \frac{c^2_0 \rho_0}{\gamma}
  \left[{\left(\frac{\rho}{\rho_0}\right)}^{\gamma} - 1\right],
\end{equation}
%
where $\gamma = 7$, $\rho_0$ and $c_0$ is the reference density and speed of
sound. In the case of a compressible fluid, the equations include an energy
equation and a different equation of state.  The discrete form of the above
equations using SPH approximation can be written as,
%
\begin{equation}
  \label{eqn:continuity:sph}
  \frac{d\rho_i}{dt} = \sum_{j} m_j (\ten{v}_i - \ten{v}_j) \cdot
  \nabla_i W_{ij},
\end{equation}
%
\begin{equation}
  \label{eq:mom:sph}
  \frac{d\ten{u}_i}{dt} = \ten{a}_{\text{p}} + \ten{a}_{\text{visc}}
  + \ten{a}_{\text{body}},
\end{equation}
where $\rho_i$, $\ten{u}_i$ is respectively the density and velocity of the
$i^{\text{th}}$ particle. $\ten{a}_{\text{p}}$ is the acceleration due to
pressure gradient commonly discretized~\cite{monaghan92} as,
%
\begin{equation}
  \label{eq:mom:sph:press}
  \ten{a}_{\text{p}} = - \sum_{j} m_j
  \left( \frac{p_i}{\rho_i^2} + \frac{p_j}{\rho_j^2} \right) \nabla_i W_{ij},
\end{equation}
%
where $p_i$ is pressure of $i^{\text{th}}$ particle, $p_j$ is pressure, $m_j$ is
the mass, and $\rho_j$ is the density of the $j^{\text{th}}$ particle,
$\nabla_i W_{ij} = \frac{\partial W}{\partial \ten{x}_i}(\ten{x}_i - \ten{x}_j,
h)$ is the gradient of the smoothing kernel. $\ten{a}_{\text{visc}}$ is the
acceleration due to viscous forces is~\cite{monaghan-review:2005},
%
\begin{equation}
  \label{eq:mom:sph:visc}
  \ten{a}_{\text{visc}} = \sum_{j} m_j \frac{4 \nu}{(\rho_i + \rho_j)}
  \frac{\ten{r}_{ij} \cdot \nabla_i W_{ij}}{(|\ten{r}_{ij}|^{2} + \eta
      h^{2})} \ten{u}_{ij},
\end{equation}
%
where $\ten{r}_{ij} = \ten{x}_i - \ten{x}_j$, and $\ten{a}_{\text{body}}$ is the
acceleration due to external body forces. Finally, the particle positions are
obtained by integrating in time the following ODE,
%
\begin{equation}
  \label{eq:eom}
  \frac{d \ten{x}_i}{dt} = \ten{u}_i.
\end{equation}
%
The problem we are going to consider in the next section is the simulation of
a two-dimensional dam break~\cite{wcsph-state-of-the-art-2010} where a column
of fluid is placed in a tank and simulated under the influence of gravity. For
the purpose of illustration we consider a simple treatment of the boundary
conditions. The tank is represented by solid particles having the same mass as
the fluid particles on which we solve the continuity equation and then set the
pressure by using the equation of state equation~\eqref{eq:eos}.

\subsection{Time Integration}
The equations of motion and momentum equations are then integrated in time to
obtain the positions and velocities respectively, a Predictor-Corrector
integrator~\cite{monaghan-review:2005} is used to integrate in time which is
described as,
%
\begin{equation}
  \ten{u}^{n+\frac{1}{2}}_i = \ten{u}^{n}_i + \frac{\Delta t}{2}{\left(\frac{d
              \ten{u}_i}{dt}\right)}^n,
\end{equation}
%
\begin{equation}
  \ten{x}^{n+1}_i = \ten{x}^{n}_i + \Delta t\ten{u}^{n + \frac{1}{2}}_i,
\end{equation}
%
\begin{equation}
  \ten{u}^{n+1}_i = \ten{u}^{n+\frac{1}{2}}_i + \frac{\Delta
    t}{2}{\left(\frac{d \ten{u}_i}{dt}\right)}^{n + 1}.
\end{equation}
%
The time-step for the integration is based on a CFL criterion that depends on
the differential equations being solved.  In the SPH literature, implicit
time-stepping is not very common as a result PySPH does not currently support
these. The issue of stiffness of the system is also not explicitly handled by
PySPH. However, it is useful to note that PySPH does support iterative
solution of matrices using a matrix-free formulation and this is discussed in
a little more detail in section~\ref{sec:iterated_group}.

\subsection{Nearest neighbor particle search}
\label{sec:nnps}

In order to find the particles that fall under the support of a kernel, a
nearest neighbor particle searching (NNPS) algorithm is used. The uniform grid
data structure is the most commonly used approach for NNPS. It works by
dividing the particles into cells of size equal to the support radius. The
neighbors of a particle are then searched in the neighboring $3^d$ boxes of
the query particle as shown in figure~\ref{fig:nnps}, where $d$ is the number
of dimensions. This method works efficiently when the support radius is
constant for all particles, but when the support radius is variable, the cell
size needs to be the maximum support radius which makes the uniform grid based
approach inefficient as the cells become too large. To efficiently find
neighbors in cases of variable support radius, a data structure with adaptive
cell size needs to be used. The most common approach in these cases is to use
an Octree~\cite{Hernquist_1989,springel_2005}. As part of PySPH, we provide
the following algorithms for NNPS on the CPU:

\begin{sloppypar}
  \begin{itemize}
  \item \textbf{Spatial Hash}. In this method, each cell in the uniform
    grid is mapped to an index in a hash table. Using a hash table allows
    storing of only the non-empty cells at the cost of a poor cache
    performance.
  \item \textbf{Linked List}~\cite{Dominguez_2011}. This method maps each cell
    to a flattened 1D cell ID which is mapped into a \code{head} array which
    stores the first particle of that cell. Another array \codeblack{next}
    maps each particle to the next particle in that cell. This algorithm gives
    a better cache performance because of its compactness, but needs to
    allocate an array of size equal to the number of cells in the domain
    including the empty cells.
  \item \textbf{Z-Order Space Filling Curve}. Space filling curves are used to
    map 3D and 2D data to one-dimension while preserving spatial locality. A
    Z-Order SFC is constructed by sorting the particle IDs by keys that are
    generated using bit-interleaving~\cite{morton_1966}. This method gives the
    best cache performance in uniform grids, but requires a sort in the build
    step which makes it slower overall than the linked list NNPS.
  \item \textbf{Octree}. In this method we recursively divide the space into 8
    octants until a node has less than a specified number of particles. When
    querying neighbors, the tree is traversed in a top-down order and only the
    octants that intersect with the bounding box of the query region are
    traversed.
  \end{itemize}
\end{sloppypar}
In addition to these, there are a few experimental NNPS algorithms implemented
in PySPH which will be discussed in a later publication. Due to its highly
parallel nature and a number of other restrictions, not all of the above
algorithms can be efficiently implemented on a GPU. PySPH provides the Z-Order
SFC, and the Octree algorithms for NNPS on the GPU.


\begin{figure}[!h]
  \centering
  \includegraphics[width=0.4\textwidth]{images/NNPS}
  \caption{Nearest neighbors of particle $q$ with support radius $h$ can be
    found in the 9 neighboring boxes of the cell that $q$ belongs to.}
\label{fig:nnps}
\end{figure}


\subsection{Schemes}

The SPH method can be used to simulate a wide variety of problems. There are
many schemes and variants of the basic scheme. The PySPH framework provides an
implementation of many of these.
%
\begin{itemize}
\item In the weakly compressible SPH family, PySPH implements the classic
  scheme of \cite{sph:fsf:monaghan-jcp94}, the corrections
  from~\cite{hughes-graham:compare-wcsph:jhr:2010}, the tensile instability
  correction~\cite{sph:tensile-instab:monaghan:jcp2000}, the
  $\delta$-SPH~\cite{marrone-deltasph:cmame:2011} method, the Transport
  Velocity Formulation (TVF)~\cite{Adami2013}, the Generalized TVF
  (GTVF)~\cite{zhang_hu_adams17}, the Entropically Damped Artificial
  compressibility (EDAC)~\cite{edac-sph:cf:2019}, the Dual time SPH
  (DTSPH)~\cite{pr:dtsph:2019}. Equations modeling surface
  tension~\cite{morris:surface:tension:2000, adami:surface:tension:2010} are
  also included in the framework.

\item In the family of Incompressible SPH (ISPH) schemes, the projection
  method of~\cite{sph:psph:cummins-rudman:jcp:1999}, the Implicit ISPH
  (IISPH)~\cite{iisph:ihmsen:tvcg-2014}, the simple iterative ISPH
  (SISPH)~\cite{muta2019simple}.

\item In the family of compressible SPH schemes, the Godunov SPH
  (GSPH)~\cite{inutsuka2002}, and the approximate Riemann solver with Godunov
  SPH~\cite{puri2014}, the adaptive density kernel estimate (ADKE)
  method~\cite{sigalotti2006}, the Monaghan-Price-Morris (MPM)
  method~\cite{price_2012}, and the conservative reproducible kernel
  SPH~\cite{crksph:jcp:2017}.

\item For elastic dynamics PySPH implements the method of
  Gray-Monaghan~\cite{gray2001}, the Generalized TVF
  (GTVF)~\cite{zhang_hu_adams17}, two way rigid-fluids
  coupling~\cite{akinci2012} and rigid body dynamics.

\item For the shallow water equations, PySPH implements the dynamic particle
  splitting and merging~\cite{vacondio_shallow_2013, vacondio_accurate_2012},
  Corrected SPH (CSPH)~\cite{rodriguezpaz_2005}, and the Lax-Friedrichs flux based
  stabilization term~\cite{ata_2005}.

\item Additional improvements and corrections that are available are the
  kernel and gradient corrections of~\cite{bonet_lok:cmame:1999} and the
  shifting algorithms
  of~\cite{acc_stab_xu:jcp:2009,fickian_smoothing_sph:skillen:cmame:2013}.

\item Boundary conditions are also implemented in the framework which
  include, wall boundary conditions~\cite{Adami2012}, inlet boundary
  conditions and various outlet boundary conditions which includes, Do
  nothing~\cite{FEDERICO201235}, Hybrid~\cite{negi2019improved}, Method of
  characteristics~\cite{Lastiwka2009:nonrefbc}, Mirror~\cite{tafuni2018}
  boundary conditions.

\end{itemize}

We next discuss the implementation, usage and design of PySPH in considerable
detail.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% End:
