\section{Sample results}
\label{sec:results}

In this section, we demonstrate the capabilities of PySPH by solving a wide
variety of problems in different fields, including incompressible and
compressible fluid dynamics, and elastic dynamics. At the end of this section
we provide qualitative indicators analysing the performance of PySPH on CPU
(both single and multi-cores) and GPU architectures. The results presented
here are only indicative of the breadth of problems that can be handled. We do
not provide all the details of the parameters used in the simulations.
However, the source code for the simulations presented are available at the
repository \url{https://gitlab.com/pypr/pysph_paper}.

\subsection{Taylor-Green Vortex}

Taylor-Green vortex is a standard test case for incompressible flows. This
problem is periodic in both $x$ and $y$ directions and has no boundaries. This
problem admits an exact solution given by,
%
\begin{equation*}
\begin{split}
  u & = -U e^{bt} \cos(2 \pi x) \sin(2 \pi y), \\
  v & = U e^{bt} \sin(2\pi x) \cos(2\pi y), \\
  p & = -U^2 e^{2bt} (\cos(4\pi x) + \cos(4\pi y))/4,  \\
\end{split}
\end{equation*}
%
where $U$ is the maximum velocity, $b=-8 \pi^2 / Re$, and $Re$ is the Reynolds
number. In order to simulate this problem, we consider a periodic domain of
size $1m \times 1m$ and use the Entropically Damped Artificial Compressible
SPH (EDAC-SPH) scheme~\cite{edac-sph:cf:2019}. The maximum velocity, $U$ and
Reynolds number, $Re$ are set to $1$m/s and $100$ respectively. The domain is
discretized by $100\times 100$ particles, and $h/\Delta x = 1.2$, where
$\Delta x$ is the particle spacing. In Fig.~\ref{fig:tg}, we plot the velocity
and pressure at $t=2$ seconds. The initial pattern is maintained, with the
decay in velocities as expected. In Fig.~\ref{fig:decay}, we plot the maximum
velocity compared with the expected theoretical result and the results are in
agreement.
%
\begin{figure}[!h] \centering
  \begin{subfigure}{0.45\linewidth}
    \includegraphics[width=1.0\linewidth]{figures/taylor_green/tg_vel}
  \end{subfigure}
%
  \begin{subfigure}{0.45\linewidth}
    \includegraphics[width=1.0\linewidth]{figures/taylor_green/tg_p}
  \end{subfigure}
\\
\caption{Velocity magnitude, and pressure distribution for Taylor-Green
  vortex at $t=2$s using EDAC~\cite{edac-sph:cf:2019}.}\label{fig:tg}
\end{figure}
%
\begin{figure}[h!]
  \centering
  \includegraphics[width=0.5\textwidth]{figures/taylor_green/decay}
  \caption{Maximum velocity decay in Taylor-Green vortex compared with the exact
    solution.}\label{fig:decay}
\end{figure}

\subsection{Lid-driven cavity}
%
The lid-driven cavity is a classic benchmark problem. We consider a 2D cavity
of size $1m\times 1m$ with the top boundary moving with a velocity $U=1$m/s
and use the Transport Velocity Formulation (TVF)~\cite{Adami2013} scheme. The
Reynolds number, $Re=1000$ is considered. $50 \times 50$ particles are used to
discretize the problem and $h/\Delta x = 1.0$. The quintic spline kernel is
used. In Fig.~\ref{fig:ldc}, we compare the velocity profile at the centerline
with \citet{ldc:ghia-1982}. The results are in agreement showing the validity
of the TVF scheme for this problem.
%
\begin{figure}[h!]
  \centering
  \includegraphics[width=0.5\textwidth]{figures/lid_driven_cavity/centerline}
  \caption{Velocity profiles $u$ vs. $y$ and $v$ vs. $x$ for the lid-driven-cavity
    problem at Re = 1000. Particle discretization of $100 \times 100$. We
    compare the results with those of~\citet{ldc:ghia-1982}.}\label{fig:ldc}
\end{figure}


\subsection{Flow past a bluff body}
Here we simulate the flow past a circular cylinder in an internal flow. This
test case is used to demonstrate the implementation of inlet outlet boundary
conditions in SPH. The inlet velocity is set to $1$m/s, the diameter of the
cylinder is $2$m, and the Reynolds number is 200. The domain is discretized
such that there are $20$ particles along the diameter of the cylinder. The
inlet continuously feeds the particles into the fluid domain with desired
initial properties, while the outlet removes the fluid particles smoothly from
the flow. Further details of the theory and implementation can be found
at~\cite{negi2019improved}.

Fig.~\ref{fig:fps} shows the velocity magnitude of the flow past the cylinder.
The vortex shedding at time $t = 90$s is seen clearly. As a quantitative
validation we determine the lift and drag coefficients for the cylinder, which
have been found to be $1.524$ and $0.722$. There is a 5\% difference from
those reported by~\cite{tafuni2018}. The Strouhal frequency is found as 0.2
and this differs from other reported results by 2\%.
%
\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{figures/flow_past_cylinder/pv.pdf}
  \caption{The plots showing the velocity magnitude of the flow past a
  circular cylinder at $t = 50.0$s and at $t = 90.0$s.}\label{fig:fps}
\end{figure}

\subsection{Dam break in 3D}

A three-dimensional dam break is simulated using a simple iterative ISPH
(SISPH) scheme which is discussed in greater detail in~\citet{muta2019simple}.
This is an iterative incompressible scheme which uses the iterated \code{Group}
feature as discussed in Section~\ref{sec:iterated_group}. A fluid column is
released from one side of a tank under the influence of gravity, an obstacle is
placed in the path of the fluid flow. In Fig.~\ref{fig:db3d}, we show the
$x$-component of the velocity at different time steps. As can be seen, the flow
splashes at the obstacle.
%
\begin{figure}[!h] \centering
  \begin{subfigure}{0.32\linewidth}
    \includegraphics[width=1.0\linewidth]{figures/dam_break_3d/sisph_u_0}
  \end{subfigure}
%
  \begin{subfigure}{0.32\linewidth}
    \includegraphics[width=1.0\linewidth]{figures/dam_break_3d/sisph_u_1}
  \end{subfigure}
\\
  \begin{subfigure}{0.32\linewidth}
    \includegraphics[width=1.0\linewidth]{figures/dam_break_3d/sisph_u_2}
  \end{subfigure}
%
  \begin{subfigure}{0.32\linewidth}
    \includegraphics[width=1.0\linewidth]{figures/dam_break_3d/sisph_u_3}
  \end{subfigure}
  \caption{Figure showing the velocity magnitude of the particles in the three
    dimensional dam break simulation at various time instances.}\label{fig:db3d}
\end{figure}
%

\subsection{Kelvin-Helmholtz instability}

This example demonstrates the modeling of fluid instabilities and compressible
flows in PySPH. We model Kelvin-Helmholtz instability, which exhibits the
subsonic instabilities of a compressible gas. Kelvin-Helmholtz instability
manifests at the interface of two fluids moving with two different velocity
profiles. The problem is periodic in both $x$ and $y$ directions. A sinusoidal
velocity perturbation is given in the $y$-direction as given
in~\citet{Robertson:2010}. This is simulated using the conservative
reproducing kernel SPH (CRKSPH)~\cite{crksph:jcp:2017} that is available in
PySPH. The density profile of the particles is shown in Fig.~\ref{fig:kh}. We
expect to see the roll-up of the shear layer, which is captured well by the
scheme.

%
\begin{figure}[!h]
  \centering
  \includegraphics[width=0.6\textwidth]{figures/kelvin_helmholtz/kh.pdf}
  \caption{Kelvin-Helmholtz instability simulation using
    CRKSPH~\cite{crksph:jcp:2017} with $200 \times 200$ particles. Shown is the
    density $\rho$ of the particles at $t = 2$s.}\label{fig:kh}
\end{figure}
%
\subsection{Sod's shock tube}

Sod's shock tube problem, considered a classic 1D problem in compressible fluid
dynamics. PySPH has several different compressible SPH schemes to solve such
problems. Here we simulate the problem using three schemes: Adaptive Density
Kernel Estimation technique (ADKE)~\cite{sigalotti2006}, Monaghan-Price-Morris
(MPM) scheme~\cite{price_2012}, Gudunov type Smoothed Particle Hydrodynamics
(GSPH)~\cite{puri2014}. For the simulation we use $720$ particles of which $640$
are placed with uniform spacing in the domain $x \in [-0.5, 0)$ and $80$ are
placed with uniform spacing in the domain $x \in [0, 0.5]$.
$(\rho_l,p_l,v_l)=(1.0,1.0,0.0)$ and $(\rho_r,p_r,u_r)=(0.125,0.1,0)$ on the
left and right sides of the diaphragm which is placed at $x = 0$. The results of
the simulation are compared with the exact solution in
Fig.~\ref{fig:shocktube}. For a detailed discussion of this problem, see
\cite{puri2014}.
%
\begin{figure}[!h] \centering
  \begin{subfigure}{1.0\linewidth}
    \includegraphics[width=1.0\linewidth]{figures/sod_shock_1d/sod_shock_1d.pdf}
  \end{subfigure}
  \caption{1D Sod's shock tube at $t=0.1$. Three schemes, ADKE, MPM and GSPH are
    used for the simulation. Comparison of the exact solution vs profiles of
    density, pressure, velocity, and energy of all the particles is shown.
  }\label{fig:shocktube}
\end{figure}

\subsection{Elastic deformation of colliding rings}
%
In this example we simulate collision of two rubber rings. This example is used
to demonstrate the ability of PySPH in modelling elastic dynamics. We have
implemented the formulation proposed by~\citet{gray2001} to model the
elastic dynamics. Two rings are initialized with a velocity such that they are
moving towards each other. Figure~\ref{fig:rings} shows the velocity magnitude
at two different times. At $t = 20\mu$s the rings have collided with each other
and start to deform. At $50\mu$s, more significant deformation is seen,
eventually the two rings reverse their velocities although this is not shown in
the figure.

%
\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{figures/solid_mech/rings.pdf}
  \caption{Velocity magnitude plots of the collision of elastic rings at times $t =
    20\mu$s and $t = 50\mu$s.}\label{fig:rings}
\end{figure}

\subsection{Sand in a rotating drum}
%

This example demonstrates an implementation of the Discrete Element Method (DEM)
implemented using PySPH. The motion of sand in a rotating drum is simulated.
The dynamics of the particles follow the formulation by Cundall and
Strack~\cite{cundall1979discrete} and Luding~\cite{luding2008introduction}. The
drum is initially static and starts rotating with an angular velocity of 5 rad/s
after the sand settles down. Fig.~\ref{fig:hopper} shows the arrangement of the
sand particles, the top row depicts the results when the drum is not rotating,
while the bottom row is when the drum is rotating. This shows that PySPH
provides the features required to implement a variety of different meshless
methods.
%
\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{figures/hopper/hopper.pdf}
  \caption{Positions of the sand in a drum, color indicates the velocity
    magnitude. The top row depicts the results when the drum is not rotating,
    and the bottom row is when the drum is rotating.}\label{fig:hopper}
\end{figure}

\subsection{Performance analysis}

In this section we discuss the performance of PySPH. Fig.~\ref{fig:cpu_flops}
shows the number of floating point operations per second (FLOPS) for a simple
simulation of a cube-shaped block of water falling in free-space under the
influence of gravity run on an i3-6100 CPU (single-core) while varying the
number of particles. We note that we compute the fluid dynamic equations on
the water particles during this computation. We use the Performance
Application Programming Interface (PAPI)~\cite{PAPI:2009} to count the FLOPS
using the CPU hardware counters.

We see that on the CPU, the performance peaks at about $2.6$ GFlop/s. Our
performance compares favorably with the singular value decomposition of an
$800 \times 800$ matrix using \code{numpy.linalg.svd} which uses the Intel
MKL~\cite{mkl_2014} implementation and is at $2.49$ GFlop/s that reduces as the
matrix size increases. It is also comparable to a 1D discrete Fourier transform
using \code{numpy.fft.fft} which also uses the MKL implementation and shows a
peak performance of $4.18$ GFlop/s. On the GPU, counting the number of FLOPS
accurately is more difficult. We measure the performance of the \code{loop}
kernel that evaluates the equations for each pair of source and destination
particles, which in the current example is the most expensive kernel call and
takes 60\% of the total execution time. Fig.~\ref{fig:gpu_flops} shows the FLOPS
for the \code{loop} kernel on a Tesla T4 GPU using single and double
precision. This performance peaks at about 180 GFlop/s using single precision,
and about $80$ GFlop/s using double precision. We note that at this point our
GPU code still requires optimizations and that the performance will be improved
in the future.
%
\begin{figure}[!h]
  \centering
  \includegraphics[width=0.6\textwidth]{images/cpu_flops}
  \caption{FLOPS vs.\ number of particles for a cube shaped block of water in
  free-fall on an i3-6100 CPU.}\label{fig:cpu_flops}
\end{figure}
%
\begin{figure}[!h]
  \centering
  \includegraphics[width=0.6\textwidth]{images/gpu_flops}
  \caption{FLOPS vs.\ number of particles for the \code{loop} kernel of a cube
    shaped block of water in free-fall using single and double precision on a
    Tesla T4 GPU.}\label{fig:gpu_flops}
\end{figure}
%
We show the performance and scale-up of PySPH on multi-core CPUs, and GPUs by
comparing the time taken to simulate the dam break 3D problem discussed earlier
in this section with varying number of particles on different platforms. We
obtain the scale-up seen with respect to the time taken on a single-core CPU. We
use an Intel i5-7400 CPU with 4 physical cores, an NVIDIA 1050Ti GPU, and an
NVIDIA 1070Ti GPU. The speed-up obtained is shown in the
Fig.~\ref{fig:speedup}. It can be seen that on a CPU with OpenMP there is nearly
a 4 fold speed up. On the GPU with the same source code we obtain speed-up of
around 17 and 32. It is to be noted that the GPU simulations are made with
single precision.
%
\begin{figure}[!h]
  \centering
  \includegraphics[width=0.6\textwidth]{figures/dam_break_3d_perf/speedup.pdf}
  \caption{The comparison of number of particles vs.\ speed-up on different
    platforms with respect to the time taken by single core CPU for the three
    dimensional dam break.}\label{fig:speedup}
\end{figure}

The multi-core CPU support in PySPH is more mature than the GPU support. To
demonstrate its efficiency, we consider a 3D dam-break problem with $0.17$M
and $1.4$M particles and measure the speed-up obtained when using OpenMP.
Fig.~\ref{fig:omp_perf} shows the results as the number of threads are
increased on a machine with dual-Intel Xeon E5-2650 v3 CPUs resulting in a
total of 20 physical cores. We obtain a speed-up of around $18$ with the 20
cores for both the problem sizes. This shows that the OpenMP implementation
scales well.

We show the speed-up with MPI for the same problem in the
Fig.~\ref{fig:mpi_perf} with $5.5$M particles. We obtain a speed-up of $55$ with
80 MPI processes on a Cray cluster (with Cray OS, 2X Intel Skylake 6148 2.4 GHz
20C processors and Cray Aries with Dragonfly topology interconnect network) with
respect to a single-core simulation. This shows that PySPH performance scales
reasonably well when run in a distributed mode. We would like to add that this
aspect of PySPH can be further optimized and this is something that will be done
in the future.
%
\begin{table}[!ht]
\centering
\begin{tabular}{lll}
  \toprule
  & PySPH v1.0b1 & DualSPHysics v5.0 \\
  \midrule
  1-core (in secs)      & 329.55       & 146.63 \\
  OpenMP 4-cores (in secs) & 90.82        & 41.26 \\
  \bottomrule
\end{tabular}
\caption{Comparison of PySPH with DualSPHyics~\cite{crespo2015dualsphysics} on
  single-core CPU, and with OpenMP on
  4-cores.}\label{tab:comparison:dualsphysics}
\end{table}
%
In order to see how well PySPH performs with a well-established, fast
implementation, we compare the performance of the CPU execution with that of
DualSPHysics v5.0~\cite{crespo2015dualsphysics} on a Intel i5-7400 single and
multi-core setup. In Table~\ref{tab:comparison:dualsphysics} we note that our
performance is around twice as slow. This is because DualSPHysics
uses a customized loop to compute the neighbors and the particle accelerations
in one step. Although this is fast, it does not allow us to replace the NNPS
algorithm easily or perform iterated evaluations of equations efficiently. We
are contemplating using this approach in the future for the weakly-compressible
schemes in PySPH. Our design allows us to implement this transparently.

In this section, we have demonstrated that the automatic code generation
produces code that is easy to write but still retains good performance with
good scaling on OpenMP and MPI. The code can be executed on a GPU, however,
the performance still requires further optimization.
%
\begin{figure}
  \centering
  \includegraphics[width=0.6\textwidth]{images/omp_speedup}
  \caption{The OpenMP speed-up with respect to single core CPU with 0.17M and
    1.4M particles in a 3D dam-break problem.}\label{fig:omp_perf}
\end{figure}
%
\begin{figure}
  \centering
  \includegraphics[width=0.6\textwidth]{images/mpi_speedup}
  \caption{The MPI speed-up with respect to single core CPU with 5.5M particles
    in a 3D dam-break problem.}\label{fig:mpi_perf}
\end{figure}
%
%%% Local Variables:solid_mech
%%% mode: latex
%%% TeX-master: "paper"
%%% End:
