"""
Flow past cylinder
"""
import os
import numpy as np

from pysph.sph.equation import Equation
from pysph.base.utils import get_particle_array
from pysph.solver.application import Application
from pysph.tools import geometry as G
from pysph.sph.bc.inlet_outlet_manager import (
    InletInfo, OutletInfo)
from pysph.sph.scheme import add_bool_argument
from pysph.examples.flow_past_cylinder_2d import WindTunnel

# local files
from isph import ISPHScheme

# Fluid mechanical/numerical parameters
rho = 1000
umax = 1.0
c0 = 10 * umax
p0 = rho * c0 * c0


class FlowPastSquare(WindTunnel):
    def _create_solid(self):
        dx = self.dx
        h0 = self.h
        sb2 = self.dc / 2
        x, y = np.mgrid[-sb2+dx/2:sb2:dx, -sb2+dx/2:sb2:dx]
        x, y = (t.ravel() for t in (x, y))
        x += self.cxy[0]
        volume = dx*dx
        solid = get_particle_array(
            name='solid', x=x, y=y,
            m=volume*rho, rho=rho, h=h0, V=1.0/volume)
        return solid


if __name__ == '__main__':
    app = FlowPastSquare()
    app.run()
    app.post_process(app.info_filename)
