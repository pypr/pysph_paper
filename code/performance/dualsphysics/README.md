#### Performance comparison of PySPH v1.0b1 with DualSPHysics v5.0.

To do this performance comparison you need to download the DualSPHysics
software from their website: https://dual.sphysics.org. Extract the file
and `cd` into the directory. Then modify the permissions appropriately
by running:

    $ bash chpermissions.sh

Next, set the environment variable DUALSPHYSICS to the bin directory
containing the software, on linux, by doing,

    $ export DUALSPHYSICS="path/to/DualSPHysics_vX.X/bin/linux/"

Then run the file `run.sh` file in this directory:

    $./run.sh

This will print the time taken to run the Dam break 3D simulation in
both PySPH and DualSPHysics on both single-core and multi-core. By
default the number of threads to run the OpenMP case is set to 4. To
change this modify the `ompthreads`  variable in the `run.sh` file. We
tried to keep all the parameters as close to each other as possible.
