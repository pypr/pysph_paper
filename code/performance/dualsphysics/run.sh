#!/usr/bin/env sh

echo "Running PySPH on a single-core..."
python ./db3d_verlet.py --max-steps 200 --kernel WendlandQuintic 2>&1 | grep "Run took"
echo "Running DualSPHyics on single-core..."
./xCaseDambreakPerf_linux64_CPU.sh -ompthreads 1 2>&1 >/dev/null | grep "Total Runtime"

ompthreads=3
echo "Running PySPH using OpenMP..."

export OMP_NUM_THREADS=$ompthreads
python ./db3d_verlet.py --openmp --max-steps 200 --kernel WendlandQuintic 2>&1 | grep "Run took"

echo "Running DualSPHyics using OpenMP..."
./xCaseDambreakPerf_linux64_CPU.sh -ompthreads $ompthreads 2>&1 | grep "Total Runtime"
