#!/usr/bin/env sh
set -e

mkdir -p runs

export OMP_NUM_THREADS=1
python -m pysph.examples.sphysics.dam_break --openmp --max-step 20 --no-adaptive-timestep --disable-output --dx 0.004 -d runs/omp_1
export OMP_NUM_THREADS=2
python -m pysph.examples.sphysics.dam_break --openmp --max-step 20 --no-adaptive-timestep --disable-output --dx 0.004 -d runs/omp_2
export OMP_NUM_THREADS=4
python -m pysph.examples.sphysics.dam_break --openmp --max-step 20 --no-adaptive-timestep --disable-output --dx 0.004 -d runs/omp_4
export OMP_NUM_THREADS=8
python -m pysph.examples.sphysics.dam_break --openmp --max-step 20 --no-adaptive-timestep --disable-output --dx 0.004 -d runs/omp_8
export OMP_NUM_THREADS=16
python -m pysph.examples.sphysics.dam_break --openmp --max-step 20 --no-adaptive-timestep --disable-output --dx 0.004 -d runs/omp_16
export OMP_NUM_THREADS=32
python -m pysph.examples.sphysics.dam_break --openmp --max-step 20 --no-adaptive-timestep --disable-output --dx 0.004 -d runs/omp_32
export OMP_NUM_THREADS=64
python -m pysph.examples.sphysics.dam_break --openmp --max-step 20 --no-adaptive-timestep --disable-output --dx 0.004 -d runs/omp_64
export OMP_NUM_THREADS=128
python -m pysph.examples.sphysics.dam_break --openmp --max-step 20 --no-adaptive-timestep --disable-output --dx 0.004 -d runs/omp_128
