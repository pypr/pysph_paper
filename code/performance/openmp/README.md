#### Performance of PySPH on a multi-core machine

To perform the analysis with multi-threads using `openmp`. install `PySPH` and
run the script by typing

    $ sh script_omp.sh

This will simulate the dam_break 3D example in `PySPH` with different threads
and save it in the `runs` folder.
