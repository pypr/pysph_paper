#### Performance of PySPH on multi-cores using MPI

To perform the analysis using MPI. install `PySPH` and configure it with
`mpi4py` and `pyzoltan`. Refer to the documentation for installation help.
This simulation has been performed on a High-performance computer. Run a
script by typing

    $ qsub 10_db_mpi.pbs 

This will simulate the dam_break 3D example in PySPH with different MPI
processes and save it in current folder. 

In order to run the simulation on a cluster, run using `mpirun`. The rest of
the command for 4 MPI processes is.


    $ mpirun -np 4 python -m pysph.examples.sphysics.dam_break --dx 0.0025
    --no-adaptive-time --disable-output --max-step 20 --ghost-layer 1 -d
    out_db_3d_10_cray_out
