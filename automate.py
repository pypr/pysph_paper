#!/usr/bin/env python
import os
import re

import json
from automan.api import PySPHProblem as Problem
from automan.api import Automator, Simulation, filter_by_name
import numpy as np
import matplotlib

from pysph.solver.utils import load, get_files, iter_output


matplotlib.use('pdf')
n_core = 4
n_thread = 4
backend = ' --openmp '


def get_files_at_given_times(files, times):
    result = []
    count = 0
    for f in files:
        data = load(f)
        t = data['solver_data']['t']
        if count >= len(times):
            break
        if abs(t - times[count]) < t*1e-8:
            result.append(f)
            count += 1
        elif t > times[count]:
            result.append(f)
            count += 1
    return result


def get_files_at_given_times_from_log(files, times, logfile):
    result = []
    time_pattern = r"output at time\ (\d+(?:\.\d+)?)"
    file_count, time_count = 0, 0
    with open(logfile, 'r') as f:
        for line in f:
            if time_count >= len(times):
                break
            t = re.findall(time_pattern, line)
            if t:
                if float(t[0]) in times:
                    result.append(files[file_count])
                    time_count += 1
                elif float(t[0]) > times[time_count]:
                    result.append(files[file_count])
                    time_count += 1
                file_count += 1
    return result


class SetupforImages(Problem):
    def set_command(self):
        return ''

    def setup(self):
        cmd = self.set_command()
        name = self.get_name()

        self.cases = [
            Simulation(
                self.input_path(name), cmd,
                job_info=dict(n_core=2, n_thread=8),
                cache_nnps=None
            )
        ]

    def run(self):
        self.make_output_dir()
        self.plot_particles()
        self.plot_prop_vs_t()

    def plot_particles(self):
        pass

    def plot_prop_vs_t(self):
        pass


def scheme_opts(params):
    if isinstance(params, tuple):
        return params[0]
    return params


class DamBreak3D(Problem):
    def get_name(self):
        return 'dam_break_3d'

    def setup(self):
        get_path = self.input_path

        cmd = 'python code/dam_break_3d.py' + backend
        tf = 1.5
        tol = 1e-2

        self.case_info = {
            'sisph': (
                dict(base_command=cmd, tf=tf, pfreq=250, tol=tol,
                     symmetric=None, gtvf=None, dx=0.01),
                dict(scheme='isph', tol=tol)
            ),
        }
        self.cases = [
            Simulation(
                get_path(name),
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, **scheme_opts(self.case_info[name])
            ) for name in self.case_info
        ]

    def run(self):
        self.make_output_dir()
        self._plot_particles()

    def _plot_particles(self):
        from mayavi import mlab
        mlab.options.offscreen = True

        for name in ('sisph',):
            fname = 'dam_break_3d'
            files = get_files(self.input_path(name), fname)
            times = [0.4, 0.6, 1.0, 1.5]
            logfile = os.path.join(self.input_path(name), 'dam_break_3d.log')
            to_plot = get_files_at_given_times_from_log(files, times, logfile)
            mlab.figure(bgcolor=(1, 1, 1), fgcolor=(0, 0, 0), size=(800, 800))
            view = None
            for i, f in enumerate(to_plot):
                data = load(f)
                t = data['solver_data']['t']
                f = data['arrays']['fluid']
                o = data['arrays']['obstacle']
                fg = mlab.points3d(
                    f.x, f.y, f.z, f.u, mode='point',
                    colormap='viridis', vmin=-2, vmax=5
                )
                fg.actor.property.render_points_as_spheres = True
                fg.actor.property.point_size = 3

                src = mlab.pipeline.builtin_surface()
                src.source = 'cube'
                src.data_source.trait_set(x_length=3.22,
                                          center=(1.61, 0.0, 0.5))
                s = mlab.pipeline.surface(src)
                s.actor.property.opacity = 0.1

                mlab.scalarbar(fg, title='u', orientation='horizontal')

                og = mlab.points3d(
                    o.x, o.y, o.z, mode='point',
                    color=(0, 0, 1)
                )
                og.actor.property.render_points_as_spheres = True
                og.actor.property.point_size = 3

                if view is None:
                    view = mlab.view()
                mlab.view(*view)
                mlab.text(0.7, 0.85, f"T = {times[i]} sec", width=0.2)

                opath = self.output_path(name + f'_u_{i}.png')
                mlab.savefig(opath)
                mlab.clf()


class DamBreak3DPerf(Problem):
    def get_name(self):
        return 'dam_break_3d_perf'

    def setup(self):
        get_path = self.input_path

        cmd = 'python code/dam_break_3d.py'
        tf = 0.05
        tol = 1e-2
        self.dx = dx = [0.1, 0.04, 0.028, 0.015, 0.01]

        self._case_info = _case_info = {
            'sisph_perf_single_core': (
                dict(base_command=cmd, tol=tol, symmetric=None, gtvf=None),
                "Single Core", 'k-o',
            ),
            'sisph_perf_omp': (
                dict(base_command=cmd+' --openmp', tol=tol, symmetric=None,
                     gtvf=None),
                "OpenMP (4 threads)", 'b-o'
            ),
            'sisph_perf_gpu_1050Ti': (
                dict(base_command=cmd+' --opencl', tol=tol, symmetric=None,
                     gtvf=None),
                "GPU 1050Ti", 'g-o'
            ),
            'sisph_perf_gpu_1070Ti': (
                dict(base_command=cmd+' --opencl', tol=tol, symmetric=None,
                     gtvf=None),
                "GPU 1070Ti", 'r-o',
            ),
        }

        case_info = {
            f'{s}_dx_{d}': dict(
                dx=d, **_case_info[s][0]
            )
            for s, d in product(_case_info, dx)
        }

        self.cases = [
            Simulation(
                get_path(name),
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, no_adaptive_timestep=None,
                disable_output=None,
                tf=tf,
                **scheme_opts(case_info[name])
            ) for name in case_info
        ]

    def run(self):
        self.make_output_dir()
        self._plot_perf()

    def _plot_perf(self):
        import matplotlib.pyplot as plt

        t = {n: [] for n in self._case_info}
        for name in self._case_info:
            fname = []
            for x in self.cases:
                if name in x.name:
                    fname.append(x.name)
            for case in filter_by_name(self.cases, fname):
                with open(case.input_path('dam_break_3d.info'), 'r') as f:
                    data = json.load(f)
                with open(case.input_path('dam_break_3d.log'), 'r') as g:
                    nx = re.findall(r"Total:\ (\d+)", g.read())
                t[name].append((float(nx[0]), data['cpu_time']))
            plt.plot(*zip(*t[name]),
                     self._case_info[name][2], label=self._case_info[name][1])
        plt.xlabel("N (number of particles)")
        plt.ylabel("time (in secs)")
        plt.grid()
        plt.semilogx()
        plt.semilogy()
        plt.legend()
        plt.savefig(self.output_path("perf.pdf"), pad_inches=0)
        plt.clf()

        plt.figure()
        dx = [x for x, _ in sorted(t['sisph_perf_single_core'])]
        single_times = np.array([
            x for _, x in sorted(t['sisph_perf_single_core'])
        ])
        omp_times = np.array([
            x for _, x in sorted(t['sisph_perf_omp'])
        ])
        gpu50_times = np.array([
            x for _, x in sorted(t['sisph_perf_gpu_1050Ti'])
        ])
        gpu70_times = np.array([
            x for _, x in sorted(t['sisph_perf_gpu_1070Ti'])
        ])

        plt.plot(dx, single_times/omp_times, 'r-o',
                 label='Single core vs OpenMP (4 threads)')
        plt.plot(dx, single_times/gpu50_times, 'b-o',
                 label='Single core vs GPU 1050Ti')
        plt.plot(dx, single_times/gpu70_times, 'c-o',
                 label='Single core vs GPU 1070Ti')
        plt.semilogx()
        plt.grid()
        plt.xlabel("N (number of particles)")
        plt.ylabel("speed up")
        plt.legend(loc='best')
        plt.savefig(self.output_path("speedup.pdf"), pad_inches=0)

class Cavity(SetupforImages):
    def get_name(self):
        return 'lid_driven_cavity'

    def set_command(self):
        return 'pysph run cavity --scheme tvf --re 1000 --tf 50 --nx 100 --pfreq 5000' +\
              backend

    def plot_particles(self):
        import matplotlib.pyplot as plt
        from pysph.solver.utils import get_files, load
        files = get_files(self.input_path(self.get_name()))
        data = load(files[-1])
        pa = data['arrays']['fluid']
        u = pa.u 
        v = pa.v
        vmag = np.sqrt(u**2 + v**2)
        x0 = pa.x.copy()
        y0 = pa.y.copy()

        plt.quiver(x0, y0, u, v, vmag)

        plt.colorbar()
        filename = self.output_path('cavity_particles.png')
        plt.savefig(filename, dpi=300)
        plt.close()

    def plot_prop_vs_t(self):
        from shutil import copy 
        dir = self.input_path(self.get_name())
        src = os.path.join(dir, 'centerline.png')
        dst = self.output_path('centerline.png')
        copy(src, dst)


class TaylorGreen(SetupforImages):
    def get_name(self):
        return 'taylor_green'

    def set_command(self):
        return 'pysph run taylor_green --tf 2.0 --scheme edac --nx 100' + backend

    def plot_particles(self):
        import matplotlib.pyplot as plt
        from pysph.solver.utils import get_files, load
        files = get_files(self.input_path(self.get_name()))
        data = load(files[-1])
        pa = data['arrays']['fluid']
        u = pa.u 
        v = pa.v
        p = pa.p
        vmag = np.sqrt(u**2 + v**2)
        x0 = pa.x.copy()
        y0 = pa.y.copy()

        plt.scatter(x0, y0, c=vmag, s=8, cmap='viridis')

        plt.colorbar(label='V')
        filename = self.output_path('tg_vel.png')
        plt.savefig(filename, dpi=300)
        plt.close()

        plt.scatter(x0, y0, c=p, s=8, cmap='viridis')

        plt.colorbar(label='P')
        filename = self.output_path('tg_p.png')
        plt.savefig(filename, dpi=300)
        plt.close()

    def plot_prop_vs_t(self):
        import matplotlib.pyplot as plt
        from matplotlib.ticker import FormatStrFormatter
        data = self.cases[0].data

        t = data['t']
        decay = data['decay']
        decay_ex = data['decay_ex']
        plt.semilogy(t, decay, '--', label='computed')
        plt.semilogy(t, decay_ex, label='exact')
        ax = plt.gca()
        ax.yaxis.set_minor_formatter(FormatStrFormatter('%.1f'))
        plt.xlabel('t')
        plt.ylabel(r'$V_{max}$')
        plt.legend()
        
        filename = self.output_path('decay.png')
        plt.savefig(filename, dpi=300)
        plt.close()


class InletOutlet(SetupforImages):
    def get_name(self):
        return 'flow_past_cylinder'

    def set_command(self):
        return 'pysph run flow_past_cylinder_2d --tf 150 --dc 2.0 --nx 20' +\
              backend

    def plot_particles(self):
        import matplotlib.pyplot as plt

        fig = plt.figure(figsize=(10, 4))
        grid = plt.GridSpec(1, 2, hspace=0.3, wspace=0.3)
        ax = []
        ax.append(fig.add_subplot(grid[0, 0]))
        ax.append(fig.add_subplot(grid[0, 1]))

        times = [50, 90]
        fname = 'flow_past_cylinder_2d'
        files = get_files(self.input_path(self.get_name()))
        logfile = os.path.join(self.input_path(self.get_name()), fname + '.log')
        to_plot = get_files_at_given_times_from_log(files, times, logfile)
        for i, time in enumerate(times):
            data = load(to_plot[i])
            pa = data['arrays']['fluid']
            u = np.sqrt(pa.u*pa.u+pa.v*pa.v)
            x0 = pa.x.copy()
            y0 = pa.y.copy()

            img = ax[i].scatter(x0, y0, c=u, s=0.5, cmap='seismic', vmin=0, vmax=2.0,
                                rasterized=True)
            ax[i].set_title('t = %.1f sec'%time)
            plt.colorbar(img, ax=ax[i], pad=0, label='v')

        plt.savefig(self.output_path('pv.pdf'))
        plt.close()

    def plot_prop_vs_t(self):
        import matplotlib.pyplot as plt
        data = self.cases[0].data
        t = data['t'] #Side of the square
        cd = data['cd']
        cl = data['cl']

        cond = t > 120
        fft = np.fft.fft(cl[cond])
        ith_freq = np.argmax(abs(fft))
        freq = np.fft.fftfreq(
            t[cond].shape[-1], t[1]-t[0])
        f = freq[ith_freq]
        St = abs(f * 2.0) #side of the square
        print('Strauhaul frequency ', St)

        avg = 5
        cd = np.convolve(cd, np.ones((avg,))/avg, mode='same')
        cl = np.convolve(cl, np.ones((avg,))/avg, mode='same')

        plt.plot(t[:-avg], cd[:-avg], '-r', label=r'$c_d$') 
        plt.plot(t[:-avg], cl[:-avg], '-g', label=r'$c_l$') 
        plt.legend()
        plt.grid()
        plt.xlabel(r'$t$')
        plt.ylabel(r'$c_d/c_l$')
        filename = self.output_path('cd_cl.png')
        plt.savefig(filename, dpi=300)
        plt.close()

class SolidMechanics(SetupforImages):
    def get_name(self):
        return 'solid_mech'

    def set_command(self):
        return 'pysph run solid_mech.rings --tf 5e-5' +\
              backend

    def plot_particles(self):
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots(1, 2, figsize=(13, 6), sharey=True, gridspec_kw =
                               {'wspace':0.05})
        ax = ax.ravel()
        times = [2e-5, 5e-5]
        fname = self.get_name()
        files = get_files(self.input_path(fname), 'rings')
        to_plot = get_files_at_given_times(files, times)
        for i, time in enumerate(times):
            data = load(to_plot[i])
            pa = data['arrays']['solid']
            img = ax[i].scatter(pa.x, pa.y, c=pa.vmag, s=0.5,
                                rasterized=True)
            ax[i].set_title(r'$t = %.1f \mu sec$'%(time*1e6))
            ax[i].axis('equal')
            ax[i].set(xlim=(-0.04, 0.12), ylim=(-0.06, 0.06))
            cbar = plt.colorbar(img, ax=ax[i], pad=0.1, label='vmag',
                                format='%.0e', orientation='horizontal')

        fig.savefig(self.output_path('rings.pdf'), pad_inches=0)
        plt.close()


class StackOfCylinders(Problem):
    def get_name(self):
        return 'stack_of_cylinders'

    def get_commands(self):
        cmd = 'python code/stack_of_cylinders.py' + backend
        return [
            ("dem", cmd, dict(n_core=2, n_thread=4)),
        ]

    def run(self):
        self.make_output_dir()
        self.plot_particles()

    def plot_particles(self):
        import matplotlib.pyplot as plt

        fname = self.get_name()
        files = get_files(self.input_path('dem'), fname)
        times = [0.1, 0.2, 0.4, 0.6]
        fig, ax = plt.subplots(2, 2, figsize=(7, 4), sharex=True,
                               sharey=True)
        ax = ax.ravel()
        i = 0
        for sd, pa in iter_output(files, 'cylinders'):
            if abs(sd['t'] - times[i]) < 1e-6 and i < len(times):
                ax[i].scatter(
                    pa.x, pa.y, c=pa.rho, s=0.1, cmap='jet',
                )
                ax[i].annotate(
                    f't = {sd["t"]:.2f}', xy=(0.18, 0.08), fontsize=12
                )
                ax[i].set_xlim(0, 0.26)
                ax[i].set_ylim(0, 0.1)
                i += 1
        fig.savefig(self.output_path('soc.pdf'), pad_inches=0)
        plt.clf()


class KH(Problem):
    def get_name(self):
        return 'kelvin_helmholtz'

    def get_commands(self):
        cmd_crksph = 'pysph run gas_dynamics.kelvin_helmholtz_instability \
        --scheme crksph --nx 300' + backend
        return [
            ("crksph", cmd_crksph, dict(n_core=2, n_thread=4)),
        ]

    def run(self):
        self.make_output_dir()
        self.plot_particles()

    def plot_particles(self):
        import matplotlib.pyplot as plt

        for name in ['crksph']:
            files = get_files(self.input_path(name), 'kelvin_helmholtz')
            data = load(files[-1])
            pa = data['arrays']['fluid']
            plt.figure(figsize=(5, 4))
            plt.scatter(
                pa.x, pa.y, c=pa.rho, marker='.', s=3,
                cmap='viridis',
                rasterized=True
            )
            plt.ylabel('y')
            plt.xlabel('x')
            plt.axis('equal')
            plt.gca().set(xlim=(0, 1), ylim=(0, 1))
            plt.colorbar(label=r'$\rho$')
        plt.savefig(self.output_path('kh.pdf'), pad_inches=0)
        plt.clf()


class Hopper(SetupforImages):
    def get_name(self):
        return 'hopper'

    def set_command(self):
        return 'python code/hopper.py --dx 0.004' + backend

    def plot_particles(self):
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots(2, 2, figsize=(8, 8))
        ax = ax.ravel()
        times = [0.2, 0.37, 2.5, 5]
        fname = self.get_name()
        logfile = os.path.join(self.input_path('hopper'), 'hopper.log')
        files = get_files(self.input_path(fname), 'hopper')
        to_plot = get_files_at_given_times_from_log(files, times, logfile)
        for i, time in enumerate(times):
            data = load(to_plot[i])
            pa = data['arrays']['sand']
            da = data['arrays']['drum']
            vmag = np.sqrt(pa.u**2 + pa.v**2)
            img = ax[i].scatter(pa.x, pa.y, c=vmag, s=0.5,
                                rasterized=True)
            ax[i].scatter(da.x, da.y, c=da.m, s=0.5, rasterized=True)
            ax[i].set_title(r'$t = %.2f s$' % (time))
            ax[i].axis('equal')
            if i in (0, 1):
                ax[i].set_xticks([])
            if i in (0, 3):
                ax[i].set_yticks([])
            plt.colorbar(img, ax=ax[i], pad=0.01,
                         label='vmag', format='%.2f')

        fig.savefig(self.output_path('hopper.pdf'), pad_inches=0)
        plt.close()


class SodShock1D(Problem):
    def get_name(self):
        return 'sod_shock_1d'

    def setup(self):
        get_path = self.input_path
        self.tf = 0.15
        self.gamma = 1.4
        cmd = 'pysph run gas_dynamics.sod_shocktube' + backend
        # kernel = 'QuinticSpline'
        kernel = 'Gaussian'
        hdx = 1.2

        _case_info = {
            'mpm': [{'scheme': 'mpm'}, 'MPM'],
            'gsph': [{'scheme': 'gsph'}, 'GSPH'],
            'adke': [{'scheme': 'adke'}, 'ADKE'],
        }

        self.case_info = {
            f'{s}': dict(
                kernel=kernel, hdx=hdx, tf=self.tf, gamma=self.gamma,
                **_case_info[s][0]
            )
            for s in _case_info
        }

        self.labels = {
            f'{s}': _case_info[s][1]
            for s in _case_info
        }

        self.cases = [
            Simulation(
                get_path(name), cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread),
                cache_nnps=None, **scheme_opts(kwargs)
            ) for name, kwargs in self.case_info.items()
        ]

    def run(self):
        self.make_output_dir()
        self._make_final_plot()

    def _make_final_plot(self):
        import matplotlib.pyplot as plt
        from pysph.examples.gas_dynamics import riemann_solver
        from pysph.examples.gas_dynamics.sod_shocktube import SodShockTube

        app = SodShockTube()
        app.initialize()

        riemann_solver.set_gamma(self.gamma)

        rho_e, u_e, p_e, e_e, x_e = riemann_solver.solve(
            x_min=app.xmin, x_max=app.xmax, x_0=app.x0,
            t=self.tf, p_l=app.pl, p_r=app.pr, rho_l=app.rhol,
            rho_r=app.rhor, u_l=app.ul, u_r=app.ur, N=101
        )

        fig, ax = plt.subplots(2, 2, sharex=True)
        ax = ax.T
        ax = ax.ravel()
        data = {}
        lw = 1.0
        ax[0].plot(x_e, rho_e, 'k', lw=lw)
        ax[1].plot(x_e, u_e, 'k', lw=lw)
        ax[2].plot(x_e, p_e, 'k', lw=lw)
        ax[3].plot(x_e, e_e, 'k', lw=lw)
        c = ['m', 'b', 'r']
        m = ['.', 'x', '1']
        style = dict(markerfacecolor='none', markeredgewidth=0.4,
                     markersize=2.0, linestyle='')
        for i, scheme in enumerate(self.case_info):
            data[scheme] = np.load(self.input_path(scheme, 'results.npz'))
            d = data[scheme]
            ax[0].plot(
                d['x'], d['rho'], markeredgecolor=c[i], marker=m[i],
                label=self.labels[scheme], **style
            )
            ax[1].plot(
                d['x'], d['u'], markeredgecolor=c[i], marker=m[i],
                label=self.labels[scheme], **style
            )
            ax[2].plot(
                d['x'], d['p'], markeredgecolor=c[i], marker=m[i],
                label=self.labels[scheme], **style
            )
            ax[3].plot(
                d['x'], d['e'], markeredgecolor=c[i], marker=m[i],
                label=self.labels[scheme], **style
            )
        ax[0].set_ylim(0, 1.1)
        ax[1].set_ylim(-0.1, 1.1)
        ax[2].set_ylim(0, 1.1)
        ax[3].set_ylim(1.5, 3)
        for axl in ax:
            axl.legend(fontsize='x-small', markerscale=2)
            axl.tick_params(
                direction='in', top=True, bottom=True, labelsize='x-small',
                left=True, right=True
            )
            # axl.grid(True, linestyle='-.')
            axl.set_xlim(-0.35, 0.35)
        ax[0].set_ylabel(r'$\rho$')
        ax[1].set_ylabel(r'$u$')
        ax[2].set_ylabel(r'$p$')
        ax[3].set_ylabel(r'$e$')
        fig.subplots_adjust(hspace=0.05, wspace=0.2)
        fig.savefig(self.output_path(self.get_name() + '.pdf'),
                    bbox_inches='tight')
        plt.close()


if __name__ == '__main__':
    PROBLEMS = [
        InletOutlet,
        SolidMechanics,
        DamBreak3DPerf,
        KH,
        StackOfCylinders,
        DamBreak3D,
        Hopper,
        Cavity,
        TaylorGreen,
        SodShock1D,
    ]
    automator = Automator(
        simulation_dir='outputs',
        output_dir=os.path.join('manuscript', 'figures'),
        all_problems=PROBLEMS
    )
    automator.run()
